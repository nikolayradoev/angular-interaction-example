import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-root',
    template: `
    <a [routerLink]="['/']"> <h1>Ma Liste</h1></a>
    <router-outlet></router-outlet>
  `,
    imports: [RouterOutlet, RouterLink]
})
export class AppComponent { }
