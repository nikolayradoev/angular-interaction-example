export interface ToDoItem {
    text: string,
    id: string
}