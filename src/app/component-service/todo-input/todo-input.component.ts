import { Component } from '@angular/core';
import { ToDoService } from '../todo.service';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-todo-input',
    template: `
  <section>
    <label for="item-input">Todo : </label>
    <input id="item-input" [(ngModel)]="inputValue" (keyup.enter)="addToDoItem(inputValue)" />
    <button (click)="addToDoItem(inputValue)">+</button>
    <button (click)="createRandomTodo()">Rand</button>
  </section>`,
    styleUrls: ['./todo-input.component.css'],
    imports: [FormsModule] // nécessaire pour utiliser ngModel
})
export class TodoInputComponent {
  inputValue = '';

  constructor(private toDoService: ToDoService) { }

  addToDoItem(itemText: string) {
    if (itemText === '') return;
    this.toDoService.addToDoItem(itemText);
    this.inputValue = "";
  }

  createRandomTodo() {
    this.inputValue = `Choisir entre 0 et  ${Math.floor(Math.random() * 100)}`;
  }
}
