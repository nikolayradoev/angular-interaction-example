import { ComponentFixture, TestBed, } from '@angular/core/testing';
import { TodoItemComponent } from './todo-item.component';
import { provideRouter } from '@angular/router';

describe('TodoItemComponent', () => {
    let component: TodoItemComponent;
    let fixture: ComponentFixture<TodoItemComponent>;

    beforeEach(async () => {

        await TestBed.configureTestingModule({
            imports: [TodoItemComponent],
            providers: [provideRouter([])]
        }).compileComponents();

        fixture = TestBed.createComponent(TodoItemComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('clickItem should emit item id', () => {
        component.toDoItem = { id: "abc", text: "Test Item" };
        spyOn(component.itemClicked, 'emit');
        component.clickItem();
        expect(component.itemClicked.emit).toHaveBeenCalled();
        expect(component.itemClicked.emit).toHaveBeenCalledWith(component.toDoItem.id);

    });

});
