import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ToDoItem } from '../../utils/todo-item';
import { RouterLink } from '@angular/router';

@Component({
    selector: 'app-todo-item',
    template: `
    <section class="todo-item">
      <a [routerLink]="['/item',toDoItem.id]">
        <span>{{ index}}. {{ toDoItem.text }}</span>
      </a>
      <button (click)="clickItem()">X</button>
    </section>`,
    styleUrls: ['./todo-item.component.css'],
    imports: [RouterLink]
})
export class TodoItemComponent {

  @Input() toDoItem: ToDoItem = { id: '', text: "" };
  @Input() index: number = 0;
  @Output() itemClicked = new EventEmitter<string>();

  clickItem() {
    this.itemClicked.emit(this.toDoItem.id);
  }
}
