import { Component } from '@angular/core';
import { ToDoItem } from 'src/app/utils/todo-item';
import { ToDoService } from '../todo.service';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-details-page',
    template: `
    @if(item){
      <h1>Page de détails pour l'item #{{item.id}}</h1>
      <h2>Texte : {{item.text}}</h2>
      <div>
        <label for="new-text">Nouveau contenu:</label>
        <input id="new-text" type="text" [(ngModel)]="newText" (keyup.enter)="renameItem()" />
      </div>
    }
    @else { <h1>Cet item n'existe pas!</h1> }`,
    imports: [FormsModule]
})
export class DetailsPageComponent {

  item: ToDoItem | undefined;
  newText = '';
  constructor(private route: ActivatedRoute,
    private todoService: ToDoService
  ) {
    const itemId = this.route.snapshot.params['id'];
    this.item = this.todoService.getToDoItem(itemId);
  }

  renameItem() {
    if (!this.newText || !this.item) return;
    this.todoService.updateItem(this.item.id, this.newText);
  }
}
