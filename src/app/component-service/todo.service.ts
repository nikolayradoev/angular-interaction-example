import { effect, inject, Injectable, signal, WritableSignal } from '@angular/core';
import { ToDoItem } from '../utils/todo-item';
import { v4 as uuidv4 } from 'uuid';
import { HistoryService } from '../observable/history.service';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {

  historyService = inject(HistoryService);
  
  toDoItems: WritableSignal<ToDoItem[]> = signal([]);

  constructor() {
    const items = sessionStorage.getItem('items');
    this.toDoItems.set(items ? JSON.parse(items) : []);

    effect(() => {
      sessionStorage.setItem('items', JSON.stringify(this.toDoItems()));
      // this.toDoItems.set([]); // N'est pas permis : peut causer une boucle infinie
    });
  }

  addToDoItem(itemText: string) {
    const toDoItem = { id: uuidv4(), text: itemText };
    this.toDoItems.update(arr => [...arr, toDoItem]);
    
    this.historyService.updateHistory(toDoItem);
  }

  deleteToDoItem($event: string) {
    this.toDoItems.set(this.toDoItems().filter(x => x.id !== $event));
  }

  getToDoItem(itemId: string) {
    return this.toDoItems().find(x => x.id === itemId);
  }

  updateItem(itemId: string, newText: string) {
    const item = this.toDoItems().find(x => x.id === itemId);
    if (item) {
      item.text = newText;
    }
  }
}
