import { TestBed } from '@angular/core/testing';
import { ToDoService } from './todo.service';
import { HistoryService } from '../observable/history.service';
import SpyObj = jasmine.SpyObj;


describe('ToDoService', () => {
    let service: ToDoService;
    let serviceSpy: SpyObj<HistoryService>;
    let getItemSpy: jasmine.Spy;
    const testItems = [{ id: '1', text: 'test' }]

    beforeEach(() => {
        getItemSpy = spyOn(sessionStorage, 'getItem').and.returnValue(JSON.stringify(testItems));
        spyOn(sessionStorage, 'setItem').and.callFake(() => { });

        serviceSpy = jasmine.createSpyObj('HistoryService', ['updateHistory']);
        TestBed.configureTestingModule({
            providers: [{ provide: HistoryService, useValue: serviceSpy }]
        });
        service = TestBed.inject(ToDoService);

    });

    it('should load items from sessionStorage', () => {
        expect(service.toDoItems().length).toBe(1);
        expect(service.toDoItems()).toEqual(testItems);
    });

    it('should create an empty list if sessionStorage is empty', () => {
        getItemSpy.and.returnValue(null); 
        TestBed.resetTestingModule(); // pour re-appeler le constructeur
        service = TestBed.inject(ToDoService);
        expect(service.toDoItems().length).toBe(0);
    });

    it('addToDoItem should call updateHistory', () => {
        service.addToDoItem('test');
        expect(serviceSpy.updateHistory).toHaveBeenCalled();
    });

    it('addToDoItem should call add item to the list', () => {
        service.addToDoItem('test');
        expect(service.toDoItems().length).toBe(2);
    });

    it('change to the list should be saved in sessionStorage', () => {
        service.addToDoItem('test');
        TestBed.flushEffects(); // forcer l'effet à s'exécuter
        expect(sessionStorage.setItem).toHaveBeenCalled();
    });

    it('deleteToDoItem should remove item from the list', () => {
        service.deleteToDoItem('1');
        expect(service.toDoItems().length).toBe(0);
    });

    it('deleteToDoItem should not change list on invalid id', () => {
        service.deleteToDoItem('2');
        expect(service.toDoItems().length).toBe(1);
    });

    it('getToDoItem should return item', () => {
        expect(service.getToDoItem('1')).toEqual(testItems[0]);
    });

    it('updateItem should change item text', () => {
        const newText = 'new text';
        service.updateItem('1', newText);
        expect(service.toDoItems()[0].text).toBe(newText);
    });

    it('updateItem should not change item text on invalid id', () => {
        service.updateItem('2', 'new text');
        expect(service.toDoItems()[0].text).toBe(service.toDoItems()[0].text);
    });

});
