import { Component, computed, inject, Signal } from '@angular/core';
import { ToDoService } from '../todo.service';
import { TodoItemComponent } from '../todo-item/todo-item.component';
import { ToDoItem } from 'src/app/utils/todo-item';
import { CommonModule } from '@angular/common';

@Component({
    selector: 'app-todo-item-list',
    templateUrl: './todo-item-list.component.html',
    styleUrls: ['./todo-item-list.component.css'],
    imports: [TodoItemComponent, CommonModule]
})
export class TodoItemListComponent {

  // équivalent à constructor(private toDoService: ToDoService) {}
  private toDoService = inject(ToDoService);

  items: Signal<ToDoItem[]> = this.toDoService.toDoItems.asReadonly();
  itemsCount: Signal<number> = computed(() => {
    return this.items().filter(x => x.text.split(' ').length >= 4).length;
  });


  get toDoCount() {
    console.log('accès à toDoCount'); // Sera appelé beaucoup trop de fois
    return this.items().length;
  }

  deleteItem(id: string) {
    this.toDoService.deleteToDoItem(id);
  }
}
