import { ComponentFixture, TestBed, } from '@angular/core/testing';
import { ToDoService } from '../todo.service';
import { TodoItemListComponent } from './todo-item-list.component';
import { ToDoItem } from 'src/app/utils/todo-item';
import { Component, EventEmitter, Input, Output, signal, WritableSignal } from '@angular/core';
import SpyObj = jasmine.SpyObj;
import { TodoItemComponent } from '../todo-item/todo-item.component';

@Component({
    selector: 'app-todo-item',
    standalone: true,
    template: ''
})
class TodoItemComponentStub {
    @Input() toDoItem: ToDoItem = { id: '', text: '' };
    @Input() index = 0;
    @Output() itemClicked = new EventEmitter<string>();
}

describe('TodoItemListComponent', () => {
    let component: TodoItemListComponent;
    let fixture: ComponentFixture<TodoItemListComponent>;
    let serviceSpy: SpyObj<ToDoService>;
    let testItems: ToDoItem[];


    beforeEach(async () => {
        testItems = [{ id: "abc", text: "Test Item Avec 4 mots" }, { id: "123", text: "Test Item #2" }];

        serviceSpy = jasmine.createSpyObj('ToDoService', ['deleteToDoItem']);
        (serviceSpy.toDoItems as WritableSignal<ToDoItem[]>) = signal(testItems);
        TestBed.overrideComponent(TodoItemListComponent, {
            add: { imports: [TodoItemComponentStub] },
            remove: { imports: [TodoItemComponent] }
        }).overrideProvider(ToDoService, { useValue: serviceSpy });

        fixture = TestBed.createComponent(TodoItemListComponent);
        component = fixture.componentInstance;
        fixture.autoDetectChanges();
    });

    it('deleteItem should call deleteItem of the service', () => {
        component.deleteItem('abc');
        expect(serviceSpy.deleteToDoItem).toHaveBeenCalledWith('abc');
    });

    it('toDoCount should return the number of items in the service', () => {
        expect(component.toDoCount).toBe(2);
    });

    it('itemsCount should return the number of items with 4 words or more', () => {
        expect(component.itemsCount()).toBe(1);
    });

    it('itemsCount should update if the items change', () => {
        serviceSpy.toDoItems.update(items => [...items, { id: 'def', text: 'a b c d' }]);
        expect(component.itemsCount()).toBe(2);
    });

    it('itemsCount should not update if the items change with a short text', () => {
        serviceSpy.toDoItems.update(items => [...items, { id: 'def', text: 'a b' }]);
        expect(component.itemsCount()).toBe(1);
    });

});
