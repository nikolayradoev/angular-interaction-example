import { Component } from '@angular/core';
import { TodoItemListComponent } from '../todo-item-list/todo-item-list.component';
import { TodoInputComponent } from '../todo-input/todo-input.component';
import { HistoryComponent } from 'src/app/observable/history/history.component';

@Component({
    selector: 'app-todo-page',
    templateUrl: './todo-page.component.html',
    styleUrl: './todo-page.component.css',
    imports: [TodoInputComponent, TodoItemListComponent, HistoryComponent]
})
export class TodoPageComponent { }
