import { TestBed } from '@angular/core/testing';

import { HistoryService } from './history.service';

describe('HistoryService', () => {
  let service: HistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistoryService);
  });

  it('updateHistory should trigger toDoItemSource and toDoImportantSource', () => {
    const item = { text: 'important', id: "123" };
    const nextSpy = spyOn(service["toDoItemSource"], 'next').and.callFake(() => { });
    const nextImportantSpy = spyOn(service["toDoImportantSource"], 'next').and.callFake(() => { });

    service.updateHistory(item);

    expect(nextSpy).toHaveBeenCalledWith(item);
    expect(nextImportantSpy).toHaveBeenCalledWith(item);
  });

  it('updateHistory should not trigger toDoImportantSource if text does not contain \'important\'', () => {
    const item = { text: 'test', id: "123" };
    const nextImportantSpy = spyOn(service["toDoImportantSource"], 'next').and.callFake(() => { });

    service.updateHistory(item);

    expect(nextImportantSpy).not.toHaveBeenCalled();
  });
});
