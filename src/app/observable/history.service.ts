import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ToDoItem } from '../utils/todo-item';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  private toDoItemSource = new Subject<ToDoItem>();
  private toDoImportantSource = new Subject<ToDoItem>();

  // On expose que la parte Observable
  toDoItem$ = this.toDoItemSource.asObservable();
  toDoImportant$ = this.toDoImportantSource.asObservable();

  updateHistory(item: ToDoItem) {
    this.toDoItemSource.next(item);
    if (item.text.toLowerCase().includes('important')) {
      this.toDoImportantSource.next(item);
    }
  }
}
