import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { HistoryComponent } from './history.component';

import SpyObj = jasmine.SpyObj;
import { HistoryService } from '../history.service';
import { Subject } from 'rxjs';
import { ToDoItem } from 'src/app/utils/todo-item';

describe('HistoryComponent', () => {
  let component: HistoryComponent;
  let fixture: ComponentFixture<HistoryComponent>;
  let serviceSpy: SpyObj<HistoryService>;
  let toDoItemSource: Subject<ToDoItem>;
  let toDoImportantSource: Subject<ToDoItem>;
  let testItem: ToDoItem;


  beforeEach(async () => {

    spyOn(console, 'log').and.callFake(() => { });

    testItem = { id: "abc", text: "Test Item" };

    toDoItemSource = new Subject<ToDoItem>();
    toDoImportantSource = new Subject<ToDoItem>();

    serviceSpy = jasmine.createSpyObj('HistoryService', ['']);
    serviceSpy.toDoItem$ = toDoItemSource.asObservable();
    serviceSpy.toDoImportant$ = toDoImportantSource.asObservable();

    await TestBed.configureTestingModule({
      imports: [HistoryComponent],
      providers: [{ provide: HistoryService, useValue: serviceSpy }]
    }).compileComponents();

    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe("Added observable", () => {

    it("modifying subscription source should change history array if subscribed", () => {
      component.startHistory();
      toDoItemSource.next(testItem);

      expect(component.history.length).toBe(1);
      expect(component.history.find(x => x.includes(testItem.text))).toBeTruthy();
    });

    it("modifying subscription source should not change history array if not subscribed", () => {
      toDoItemSource.next(testItem);

      expect(component.history.length).toBe(0);
    });

    it("re-subscribing to observable should resume history without keeping intermediary values", () => {
      component.startHistory();
      toDoItemSource.next(testItem);
      component.stopHistory();
      toDoItemSource.next(testItem);

      component.startHistory();
      toDoItemSource.next(testItem);

      expect(component.history.length).toBe(2);
    });

    it("startHistory should not subscribe if already subscribed", () => {
      component.startHistory();
      expect(component['addedSubscription'].closed).toBeFalse();
      component.startHistory();
      expect(component['addedSubscription'].closed).toBeFalse();
    });

    it("stopHistory should unsubscribe from observables", () => {
      const addedSubSpy = spyOn<any>(component['addedSubscription'], "unsubscribe");
      component.stopHistory();
      expect(addedSubSpy).toHaveBeenCalled();
    });

    it('ngOnDestroy should call stopHistory', () => {
      const stopHistorySpy = spyOn(component, 'stopHistory');
      component.ngOnDestroy();
      expect(stopHistorySpy).toHaveBeenCalled();
    });
  });

  describe("Important observable", () => {
    beforeEach(() => {
      testItem = { id: "abc", text: "Message important" };
    });

    it("receiving an emit containing 'important' should change the notification message", () => {
      toDoImportantSource.next(testItem);

      expect(component.popup).toBeTrue();
      expect(component.notificationMessage).toBe(`Le message #${testItem.id} est important!`);
    });

    it("popup should be true before the end of the timer", fakeAsync(() => {
      const timer = component['DISPLAY_TIMER'] / 2;
      toDoImportantSource.next(testItem);
      tick(timer);
      expect(component.popup).toBeTrue();
      flush();
    }));

    it("popup should be set to false after end of the timer", fakeAsync(() => {
      const timer = component['DISPLAY_TIMER'];
      toDoImportantSource.next(testItem);
      tick(timer);
      expect(component.popup).toBeFalse();
      flush();
    }));



  });

});
