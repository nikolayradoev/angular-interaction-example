import { Component, inject, OnDestroy } from '@angular/core';
import { Observer, Subscription } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ToDoItem } from 'src/app/utils/todo-item';
import { HistoryService } from '../history.service';
import { EMPTY_SUBSCRIPTION } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-history',
  template: `
    <h1>Historique des items!</h1>
    <button (click)="startHistory()">Débuter l'historique</button>
    <button (click)="stopHistory()">Arrêter l'historique</button>
    @if(popup){ <p>{{notificationMessage}}</p> }
    @for(historyItem of history; track historyItem){ <p>{{ historyItem }}</p> }`
})
export class HistoryComponent implements OnDestroy {

  history: string[] = [];
  historyService = inject(HistoryService);
  private addedSubscription: Subscription = EMPTY_SUBSCRIPTION; // attribut closed = true

  private readonly DISPLAY_TIMER = 1000;
  popup = false;
  notificationMessage = "";

  startHistory() {
    if (!this.addedSubscription.closed) {
      return;
    }
    // Méthode plus verbeuse avec un objet Observer explicite
    const addedObserver: Observer<ToDoItem> = {
      next: (addedItem: ToDoItem) => {
        this.history.push(`L'item "${addedItem.text}" a été ajouté!`);
        console.log(addedItem.text); // ceci sera affiché si on ne se désabonne pas à la destruction du composant
      },
      error: () => { },
      complete: () => { }
    }
    this.addedSubscription = this.historyService.toDoItem$.subscribe(addedObserver);
  }

  stopHistory() {
    this.addedSubscription?.unsubscribe();
  }

  ngOnDestroy() {
    this.stopHistory();
  }

  constructor() {
    this.historyService.toDoImportant$
      .pipe(takeUntilDestroyed()) // Se désabonne à la destruction du composant. Évite les fuites de mémoire
      .subscribe(
        (item: ToDoItem) => {
          this.notificationMessage = `Le message #${item.id} est important!`;
          this.popup = true;
          setTimeout(() => { this.popup = false; }, this.DISPLAY_TIMER);
        });
  }

}
