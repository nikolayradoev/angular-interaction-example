import { Routes, provideRouter } from "@angular/router";
import { AppComponent } from "./app/app.component";
import { bootstrapApplication } from "@angular/platform-browser";
import { TodoPageComponent } from "./app/component-service/todo-page/todo-page.component";
import { DetailsPageComponent } from "./app/component-service/details-page/details-page.component";

const routes: Routes = [
  { path: '', component: TodoPageComponent },
  { path: 'item/:id', component: DetailsPageComponent },
];

bootstrapApplication(AppComponent, {
  providers: [provideRouter(routes)],
});
