# Angular Interaction Example

Ce projet est un tutoriel pour apprendre le fonctionnement de base d'Angular. Le projet couvre les notions de bases suivantes :

- Composantes Angular pour la gestion de la vue
  - Composantes _standalone_
  - Dépendances entre composantes
  - Composantes imbriquées
- Syntaxe Angular pour les gabarits HTML
  - Interpolation `{{}}`
  - Liaison de propriétés `[property]`
  - Liaison d'événements `(event)`
  - Liaison bidirectionnelle `[(ngModel)]`
  - Flôt de contrôle `@if`, `@for`, `@empty`
- Services Angular pour la gestion de la logique
  - Injection de dépendances
  - Gestion des données
  - Partage de service par plusieurs composantes
- Utilisation de `@Input()` et `@Output()` pour communiquer entre composantes
  - Communication Parent -> Enfant avec `@Input()`
  - Communication Enfant -> Parent avec `@Output()`
- Utilisation de `Signal API` pour communiquer entre services et composantes
  - Signal en écriture avec `signal(initValue)`
  - Signal en lecture avec `computed()`
  - Effets de changement avec `effect()`
- Utilisation de `Observable` et `Subject` pour communiquer entre services et composantes
  - Définition d'un `Observable`
  - Souscription à un `Observable` par un `Observer`
  - Utilisation de `Subject` pour notifier plusieurs `Observer`
  - Gestion de la fin de la souscription

## Liste des tâches à faire

Le projet est une application de gestion de tâches à faire. L'application permet d'ajouter des tâches à faire dans une liste affichée et de les retirer de cette liste. 

Un bouton permet de générer un nom aléatoire pour présenter la notion de liaison bidirectionnelle avec `[(ngModel)]`. L'autre bouton, ou la touche `Enter`, permet d'ajouter une tâche à la liste.

Chaque tâche est représentée dans une liste et donne accès à une page avec ses détails. La page permet de changer le texte de la tâche. Notez qu'il suffit de simplement utiliser le bouton `Enter` pour appliquer le changement.

Comme Angular permet la création d'applications monopages (_SPA_), la navigation entre ces deux pages est faite sans rechargement de la page. Ceci est fait grâce à la directive `routerLink`. Le contexte _JS_ n'est pas perdu lors de la navigation.

Ce changement est répercuté dans la liste des tâches puisque les tâches sont gardées dans un service partagé : `ToDoService`. Ceci permet de présenter la notion de singletons dans Angular. 

Le service utiliser également le nouveau API `Signal` pour notifer les componsantes d'une mise à jour de la liste des tâches. La fonction `effect()` est déclenchée à chaque fois que la liste est modifiée et sauvegarde la liste dans le `sessionStorage`. Ceci permet une certaine persistance des données.

La classe [TodoItemListComponent](./src/app/component-service/todo-item-list/todo-item-list.component.ts) et son [Gabarit HTML](./src/app/component-service/todo-item-list/todo-item-list.component.html) sont un très bon exemple de la majorité des notions de base d'Angular. Il est fortement conseillé de bien comprendre la syntaxe utilisée dans ce composant.

Notes que l'accesseur `toDoCount` contient un appel à `console.log` et est un exemple des limitations de la vérification des changement d'Angular. L'accesseur est appelé à chaque changement de la liste des tâches, même si le nombre de tâches n'a pas changé. L'API `Signal` permet de résoudre ce problème en ne notifiant que les composantes qui ont besoin de savoir qu'un changement a eu lieu. Il est donc conseillé de retirer l'appel à la console une fois que vous avez compris l'enjeu pour éviter de polluer la console.

## Historique, items importants et les Observables

L'application permet également de voir l'historique des actions effectuées. Ceci est fait grâce à [HistoryComponent](./src/app/observable/history/history.component.ts) qui utilise un service [HistoryService](./src/app/observable/history.service.ts) pour garder l'historique des actions.

Les 2 classes communiquent à travers des `Observables`. Un `Observable` permet de notifier le composant de l'arrivée d'un nouvel élément dans l'historique. Un deuxième permet de notifier le composant seulement si un item ayant le texte `important` est ajouté à la liste des tâches.

La composante présente deux manières différentes de gérer la fin des abonnements à un Observable. Notez qu'il est très important de bien gérer la fin des abonnements pour éviter des fuites de mémoire.

## Installation et lancement

Les dépendances du projet peuvent être installées avec la commande `npm ci`. Le fichier `package-lock.json` définit les versions exactes à utiliser.

Le projet peut être lancé localement avec la commande `npm start`. Cette commande va transpiler le code source et utiliser un serveur de développement statique pour servir l'application web sur le port **4200**. Votre navigateur ouvrira automatiquement une page à l'adresse `http://localhost:4200` si la compilation a été réussie.

## Tests unitaires

La majorité du code source est accompagné de test unitaires pour vérifier le bon fonctionnement des différentes fonctionnalités. Les tests sont écrits avec Jasmine et exécutés avec Karma.

Les tests de la classe [ToDoService](./src/app/component-service/todo.service.spec.ts) présentent la manière de tester les `signals` ainsi que la fonction `effect()`. La configuration est également un exemple de gestion des dépendances d'un service.

Les tests de la classe [TodoItemComponent](./src/app/component-service/todo-item/todo-item.component.spec.ts) présentent la manière de tester les interactions entre les composants à travers les `@Input()` et `@Output()`.

Les tests de la clase [HistoryComponent](./src/app/observable/history/history.component.spec.ts) présentent la manière de tester les `Observables`. Ces tests incluent un exemple de tests impliquant des délais simulés à l'aide des méthodes `fakeAsync`, `tick` et `flush`. 

Les tests peuvent être lancés avec la commande `npm test`.

## Ressources supplémentaires

Consultez les ressources suivantes pour mieux comprendre le fonctionnement d'Angular et ses différentes fonctionnalités :

- [Angular Documentation](https://angular.dev/overview)
- [Angular Tutorial](https://angular.dev/tutorials/learn-angular)
- [Syntaxe des Gabarits](https://angular.dev/guide/templates)
- [Signals](https://angular.dev/guide/signals)
- [RxJS Observables](https://rxjs.dev/guide/overview)